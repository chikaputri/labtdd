from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, tambahstatus, profil
from .models import status
from .forms import status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class story6UnitTest(TestCase):
    
    def test_myapp_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_myapp_using_index_func(self):
      found = resolve('/')
      self.assertEqual(found.func, index)

    def test_model_can_fill_status(self):
    	new_status = status.objects.create(deskripsi='Hai')
    	counting_all_available_status = status.objects.all().count()
    	self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
      form = status_Form(data={'deskripsi': ''})
      self.assertFalse(form.is_valid())
      self.assertEqual(form.errors['deskripsi'], ["This field is required."])

    def test_myapp_post_success_and_render_the_result(self):
      test = 'Anonymous'
      response_post = Client().post('/tambahstatus', {'deskripsi': test})
      self.assertEqual(response_post.status_code, 302)

      response= Client().get('/')
      html_response = response.content.decode('utf8')
      self.assertIn(test, html_response)

    def test_lab5_post_error_and_render_the_result(self):
      test = 'Anonymous'
      response_post = Client().post('/tambahstatus', {'deskripsi': ''})
      self.assertEqual(response_post.status_code, 302)

      response= Client().get('/')
      html_response = response.content.decode('utf8')
      self.assertNotIn(test, html_response)

    def test_challenge6_url_is_exist(self):
      response = Client().get('/profil/')
      self.assertEqual(response.status_code,200)

    def test_challenge6_using_profil_func(self):
      found = resolve('/profil/')
      self.assertEqual(found.func, profil)

    def test_name_in_html(self):
      response = Client().get('/profil/')
      html_response = response.content.decode('utf8')
      self.assertIn("Chika Putri", html_response)

# Create your tests here.
class story6FunctionalTest(TestCase):

  def setUp(self):
    chrome_options = Options()
    self.selenium = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
    super(story6FunctionalTest, self).setUp()

  def tearDown(self):
    self.selenium.quit()
    super(story6FunctionalTest, self).tearDown()

  def test_input_status(self):
    selenium = self.selenium
    # Opening the link we want to test
    selenium.get('http://127.0.0.1:8000/')
    # find the form element
    deskripsi = selenium.find_element_by_name('deskripsi')

    submit = selenium.find_element_by_id('submit')

    # Fill the form with data
    deskripsi.send_keys('Selenium Test')

    # submitting the form
    submit.send_keys(Keys.RETURN)