from django.conf.urls import url, include
from .views import index, tambahstatus, profil

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'tambahstatus', tambahstatus, name='tambahstatus'),
    url(r'^profil', profil, name = 'profil'),
]