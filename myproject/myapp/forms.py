from django import forms

class status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    deskripsi_attrs = {
        'type': 'text',
        'placeholder':'Apa yang anda pikirkan...'
    }

    deskripsi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=deskripsi_attrs))