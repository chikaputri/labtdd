from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import status
from .forms import status_Form

# Create your views here.
response={}
def index(request):
    response['stats'] = status_Form
    response['status'] = status.objects.all()
    html = 'index.html'
    return render(request, html, response)


def tambahstatus(request):
    form = status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['deskripsi'] = request.POST['deskripsi']
        a = status(deskripsi=response['deskripsi'])
        a.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
		
def profil(request):
	return render(request, 'profil.html')